<%--
  Created by IntelliJ IDEA.
  User: rog
  Date: 2/28/2020
  Time: 10:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
  <head>
    <title>$Title$</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="app.css" type="text/css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>
  <body>
<%--  <%@include file="_header.jsp"%>--%>
  <c:import url="_header.jsp" var="_header"/>
  ${_header}
  <%@ page errorPage="ErrorPage.jsp" %>
  Hello World
  <form action="home" method="post">
    <p>
      <div>
        <c:out value="Helloooo" />
      </div>
        <c:choose>
            <c:when test="${ !empty user.name }">
                <h2>Welcome ${ user.name }</h2>
            </c:when>
            <c:otherwise>
                <h2>Welcome whoever you are</h2>
            </c:otherwise>
        </c:choose>
<%--      <c:if test="${ !empty user.name }">--%>
<%--        <h2>--%>
<%--          Welcome ${ user.name }--%>
<%--        </h2>--%>
<%--      </c:if>--%>
<%--      <c:if test="${ empty user.name }">--%>
<%--        <h2>--%>
<%--          Welcome whoever you are--%>
<%--        </h2>--%>
<%--      </c:if>--%>
      <ul>
          <c:forEach items="${app.tabs}" var="tab">
              <li><a href="${ tab.url }">${tab.name}</a></li>
          </c:forEach>
      </ul>
      <h2 class="${ app.formCssClass }">Welcome ${ user.name }</h2>
      <h2>${ user.name == "Kavin" }</h2>
      Name: <input type="text" name="name"/>
      <input type="submit" value="Enter name" />
    </p>
  </form>
  </body>
</html>
