import java.util.List;
import java.util.Map;

public class ApplicationSettings {
    private String _fromCssClass;

    public String getFormCssClass() { return this._fromCssClass; };

    public void setFormCssClass(String value) {
        this._fromCssClass = value;
    }

    private String[] _tabNames;

    public String[] getTabNames() {
        return this._tabNames;
    }

    public void setTabNames(String[] names) {
        this._tabNames = new String[names.length];
        System.arraycopy(names, 0,this._tabNames, 0, names.length);
    }

    private List<Tab> _tabs;
    public List<Tab> getTabs() { return this._tabs; };
    public void setTabs(List<Tab> tabs) {
        this._tabs = tabs;
    }
}
