import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = { "/home" }, name = "SimpleServlet", initParams = {@WebInitParam(name = "ProductName", value = "test")})
public class SimpleServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        String name = req.getParameter("name");
        if(name != null) {
            res.getWriter().printf("Hello %s", name);
        } else {
            throw new ServletException("A name should be provided.");
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String name = req.getParameter("name");
        String Donni = "";
        String Donni2 = "";
        Boolean compare = Donni == Donni2;
        res.getWriter().printf("$compare", compare);

        if(name != null && !name.isEmpty()) {
            res.getWriter().printf("here");
            res.getWriter().printf("Hello %s", name);
        } else {
            res.sendRedirect("index.jsp");
//            res.getWriter().printf("%s", !"");

        }
    }
}