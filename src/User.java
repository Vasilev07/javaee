package User;

public class User {
    private String _name;
    private String _email;

    public String getName(){
        return this._name;
    }

    public void setName(String value) {
        this._name = value;
    }

    public String getEmail() {
        return  this._email;
    }

    public void setEmail(String value){
        this._email = value;
    }
}
